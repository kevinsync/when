
  if (!Function.prototype.curry) {

    //      from : http://javascriptweblog.wordpress.com/2010/04/05/curry-cooking-up-tastier-functions
    // read this : http://stackoverflow.com/questions/113780/javascript-curry-what-are-the-practical-applications

    function toArray(Enum) {
      return Array.prototype.slice.call(Enum);
    }

    Function.prototype.curry = function () {
      if (arguments.length < 1) {
        return this; // nothing to curry with - return function
      }
      var __method = this;
      var args = toArray(arguments);
      return function() {
        return __method.apply(this, args.concat(toArray(arguments)));
      }
    };

  } // end if

  module.exports = {};
