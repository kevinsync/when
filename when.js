
// ----------------------------------------------------------------------------------
//  TODO: restore log() functionality
// ----------------------------------------------------------------------------------

  require("./curry.js");

//var _              = require("lodash");
  var flatten        = require("flat");
  var object_path    = require("object-path");
  var proxy_observer = require("@createvibe/proxyobserver");

// ----------------------------------------------------------------------------------

  var observed = [];

// ----------------------------------------------------------------------------------

  module.exports = function () {

// ----------------------------------------------------------------------------------

    var target, path, expression, value, func;

/* ----------------------------------------------------------------------------------

    - returns modified target with these added:

      _when: {

        enabled: true,

        changed: [

          {
            path: "" || [],
            then: function (path, was, value) {}
          }
        ],

        equals: [

          {
            path: "" || [],
            value: "" || function () {}
            then: function (path, was, value) {}
          }
        ],

        satisfies: [

          {
            path: "" || [],
            value: "" || function () {}
            then: function (path, was, value) {}
          }
        ],

        log: [], // TODO: change this to log to MySQL

      }

   ----------------------------------------------------------------------------------

    Accepts multiple parameter combinations:

      1 -

        when(object);

      2 -

        when(object, function (path, was, is) { console.log(path, was, is); });

      3 -

        when(object, "prop1", function (path, was, is) { console.log(path, was, is); });

      4 -

        when(object, "prop1", "changes", function (path, was, is) { console.log([path, was, is]); });

      5 -

        when(object, "prop1", "equals", 7, function () { console.log("equals 7"); });

      6 -

        when(object, "prop1", "satisfies", function (values) { return values["prop1"] > 3; }, function () { console.log("greater than 3"); });

   ----------------------------------------------------------------------------------
*/
    function log(path, was, is) {
/*
      if (!_log) { return; } // end if

      var path = `${changed.name}` + (changed.key ? `["${changed.key}"]` : "");
          path = nickname ? `${nickname}.${path}` : path;

      switch (changed.type) {

        case "add":
        case "update":
          console.log(`${path} = ${JSON.stringify(changed.new_value)};`);
          break;

        case "delete":
          console.log(`delete ${path};`);
          break;

      } // end switch
*/
    } // end log()

// ----------------------------------------------------------------------------------

    function setup_when(target) {

      if (!target._when) {

        target._when = {

          enabled: true, // toggle to turn evaluations on and off

          changed: [],

          equals: [],

          satisfies: [],

          log: [],

          export: function () {

            // NOTE: if lodash isn't available, fallback behavior removes anything that cannot be stringified (like classes, functions, callback, etc)

            var _self = ((typeof _ !== 'undefined') && _.cloneDeep) ? _.cloneDeep(this) : JSON.parse(JSON.stringify(this));

            delete _self._when;

            return _self;

          }.bind(target) // end export()

        };

      } // end if

    } // end setup_when()

// ----------------------------------------------------------------------------------

    function evaluate_conditions() {

      const chain = Array.prototype.slice.call(arguments);

      const { prop } = chain[0];
      const { value, oldValue } = chain[chain.length - 1];

      const old_value = oldValue;

      const path = chain.map(node => node.prop);

      const full_path = path.join(".");
/*
      console.log('the root property',
                  prop,
                  'had a nested object modified with the value',
                  full_path,
                  '=',
                  value,
                 );
*/
      // EVAL CONDITIONS HERE

      if (this._when.enabled) {

        // --------------------------------------------------------------------------
        //  changes
        // --------------------------------------------------------------------------

        this._when.changed.forEach(function (condition) {

          var conditions = !condition.path ? [] : (

            (typeof condition.path == "string") ? [condition.path] : condition.path

          );

          var matching_condition = false;

          conditions.forEach(function (condition_path) {

            if (full_path.substring(0, (condition_path || "").length) == condition_path) {

              matching_condition = true;

            } // end if

          }); // end path.forEach()

          if (!conditions.length || matching_condition) {

            if (!full_path.includes("_when")) {

              if (typeof condition.then == "function") {

                setTimeout(function () {

                  (condition.then.curry(full_path, old_value, value))();

                }, 1);

              } // end if

            } // end if

          } // end if

        }); // end changed.forEach()

        // --------------------------------------------------------------------------
        //  equals
        // --------------------------------------------------------------------------

        this._when.equals.forEach(function (condition) {

          //  Due to proxyobserver catching mutations before the object is actually updated, we
          //    duplicate the origin object and stub it out with the updated value
          //
          //  - you might pass in a object with a nested parameter as the updated value and your
          //    condition path refers to a child property of the overall value
          //
          /*      ex. obj = {
                              a: {
                                b: {}
                              }
                            }

                      obj.a.b = {c: d: name: "kevin"}

                      obj = when(obj);

                      when(obj, "a.b.c.d.name", "equals", "kevin", function () { console.log("KEVIN!"); });

              - the change is found to happen as "obj.a.b = [Object]" and "value" passes in as [Object], not "kevin"
                so you have to stub out chain[0].target with "value" and then get the value that matches condition.path
                from that temporary object instead

              - this happens with "satisfies" conditions too

          */

          var conditions = !condition.path ? [] : (

            (typeof condition.path == "string") ? [condition.path] : condition.path

          );

          var matching_condition = false;
          var matching_path = "";

          conditions.forEach(function (condition_path) {

            if ((condition_path || "").substring(0, (full_path || "").length) == full_path) {

              matching_condition = true;
              matching_path = condition_path;

            } // end if

          }); // end conditions.forEach()

          if (!conditions.length || matching_condition) {

            var proxy = chain[0].target;

            object_path.set(proxy, matching_path, value);

            var current_value = object_path.get(proxy, matching_path);

            var values = {};

            conditions.forEach(function (condition_path) {

              values[condition_path] = object_path.get(proxy, condition_path);

            }); // end conditions.forEach()

            if (typeof condition.then == "function") {

              if (typeof condition.value == "function") {

                if (JSON.stringify(current_value) == JSON.stringify(condition.value())) {

                  setTimeout(function () {

                    (condition.then.curry(matching_path || full_path, old_value, current_value))();

                  }, 1);

                } // end if

              } else {

                if (JSON.stringify(current_value) == JSON.stringify(condition.value)) {

                  setTimeout(function () {

                    (condition.then.curry(matching_path || full_path, old_value, current_value))();

                  }, 1);

                } // end if

              } // end if

            } // end if

          } // end if

        }); // end equals.forEach()

        // --------------------------------------------------------------------------
        //  satisfies
        // --------------------------------------------------------------------------

        this._when.satisfies.forEach(function (condition) {

          var conditions = !condition.path ? [] : (

            (typeof condition.path == "string") ? [condition.path] : condition.path

          );

          var matching_condition = false;
          var matching_path = "";

          conditions.forEach(function (condition_path) {

            if ((condition_path || "").substring(0, (full_path || "").length) == full_path) {

              matching_condition = true;
              matching_path = condition_path;

            } // end if

          }); // end conditions.forEach()

          if (!conditions.length || matching_condition) {

            var proxy = chain[0].target;

            object_path.set(proxy, matching_path, value);

            var current_value = object_path.get(proxy, matching_path);

            var values = {};

            conditions.forEach(function (condition_path) {

              values[condition_path] = object_path.get(proxy, condition_path);

            }); // end conditions.forEach()

            if (typeof condition.then == "function") {

              if (typeof condition.value == "function") {

                if (condition.value(values)) {

                  setTimeout(function () {

                    (condition.then.curry(matching_path || full_path, old_value, current_value))();

                  }, 1);

                } // end if

              } // end if

            } // end if

          } // end if

        }); // end satisfies.forEach()

      } // end if

    } // evaluate_conditions()

// ----------------------------------------------------------------------------------

    if (arguments.length) {

      target = arguments[0];

      var all_keys = Object.keys(flatten(target));
      var warn = false;

      all_keys.forEach(function (key) {

        if (key.includes("_when.")) {

          warn = true;

        } // end if

      }); // end forEach()

      if (!target._when) {

        setup_when(target);

        target = proxy_observer(target, evaluate_conditions.bind(target));

        if (warn) {

          console.log("WARNING: you are now observing a parent of a property that's already being observed! Events might fire multiple times while satisfying both parent and child observations.");

        } // end if

      } // end if

    } // end if

    switch (arguments.length) {

      case 1:

        expression = "changes";

        break;

      case 2:

        expression = "changes";
        func       = arguments[1];

        break;

      case 3:

        expression = "changes";
        func       = arguments[2];

        break;

      case 4:

        path       = arguments[1];
        expression = arguments[2];
        func       = arguments[3];

        break;

      case 5:

        path       = arguments[1];
        expression = arguments[2];
        value      = arguments[3];
        func       = arguments[4];

        break;

    } // end switch

    switch (expression) {

      case "changes":

        target._when.changed.push({

          path: path,
          then: func,

        });

        break;

      case "equals":

        target._when.equals.push({

          path: path,
          value: value,
          then: func,

        });

        break;

      case "satisfies":

        target._when.satisfies.push({

          path: path,
          value: value,
          then: func,

        });

        break;

    } // end switch

    return target;

  };

// ----------------------------------------------------------------------------------
