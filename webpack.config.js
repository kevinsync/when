const path = require('path');

module.exports = {
  entry: './when.js',
  output: {
    filename: 'when.bundle.js',
    path: path.resolve(__dirname),
    library: 'when',
  },
  mode: "production",
};